/*VAR*/
var i = 0, new_job;

/*RANDOMS FUNC*/
function random(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function random_time(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*TYPING FUNC*/
function typing() {
    if(i<new_job.length){
        if(new_job.charAt(i) == '\n') document.getElementById("p").innerHTML += "<br>";
    else
        document.getElementById("p").innerHTML += new_job.charAt(i);
        i++;
        setTimeout(typing,200);
    }
}

/*RESET FUNC*/
function reset(){
    document.getElementById("p").innerHTML = "I am a ";
    i = 0;
    typing();
}

function init_(){
    let word_index = random(0,10);

    switch(word_index){
        case 0:
            new_job = "Builder";
            typing();
            reset();
            break;
        case 1:
            new_job = "Firefighter";
            typing();
            reset();
            break;
        case 2:
            new_job = "Developer";
            typing();
            reset();
            break;
        case 3:
            new_job = "Scientist";
            typing();
            reset();
            break;
        case 4:
            new_job = "Star";
            typing();
            reset();
            break;
        case 5:
            new_job = "Baker";
            typing();
            reset();
            break;
        case 6:
            new_job = "President";
            typing();
            reset();
            break;
        case 7:
            new_job = "Doctor";
            typing();
            reset();
            break;
        case 8:
            new_job = "Dancer";
            typing();
            reset();
            break;
        case 9:
            new_job = "Rapper";
            typing();
            reset();
            break;
        case 10:
            new_job = "Bitch";
            typing();
            reset();
            break;
    }
    console.log(new_job);
    console.log(word_index);
};

setInterval(init_, random_time(2000, 6000));